<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use Route;

class Helper
{

    public static function areActiveRoutes(Array $routes, $output = "active")
    {
        foreach ($routes as $route) {
            if (Route::currentRouteName() == $route) return $output;
        }
    }

    public static function getFileBaseURL()
    {
        if(env('FILESYSTEM_DRIVER') == 's3'){
            return env('AWS_URL').'/';
        }
        else {
            return Helper::getBaseURL().'/';
        }
    }

    public static function getBaseURL()
    {
        $root = (Helper::isHttps() ? "https://" : "http://").$_SERVER['HTTP_HOST'];
        $root .= str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

        return $root;
    }

    public static function isHttps()
    {
        return !empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS']);
    }

    public static function isAdmin()
    {
        return auth()->user()->role=='admin';
    }

    public static function getColor($status)
    {
        switch($status){
            case "in_process":
                return "badge-secondary";
            break;
            case "pending":
                return "badge-warning";
            break;
            case "sent":
                return "badge-success";
            break;
            case "cancelled":
                return "badge-danger";
            break;
        }
    }
}