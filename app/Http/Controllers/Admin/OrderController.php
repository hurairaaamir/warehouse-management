<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use App\Product;
use App\ProductType;
use App\Notifications\NewOrderNotification;
use Helper;
use App\Events\MailEvent;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Helper::isAdmin()){
            $orders=Order::where('type','send')->get();
        }else{
            $orders=Order::where('user_id',auth()->id())->where('type','send')->orderBy('id','DESC')->get();
        }
        return view('backend.orders.index_order',compact('orders'));
    }

    public function new_index(){
        if(Helper::isAdmin()){
            $orders=Order::where('type','new')->get();
        }else{
            $orders=Order::where('user_id',auth()->id())->where('type','new')->orderBy('id','DESC')->get();
        }
        return view('backend.orders.index_new_order',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.orders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "amount"=>"required",
            "user_id"=>"required",
            "product_type_id"=>"required",
            "price"=>"required"
        ]);
        $product_type=ProductType::findOrFail($request->product_type_id);
        if($request->type=='send'){
            if($product_type->quantity<$request->amount){
                flash('stock not enough in warehouse')->error();
                return back();
            }
        }

        
        $order=Order::create($request->all());

        $this->sendNotification($request);

        $this->ToAdminEmail();
        $this->ToSellerEmail();
        
        flash('order has been created successfully')->success();
        
        return ($request->type=='new')?redirect()->route('orders.new.all'):redirect()->route('orders.all');   
    }

    public function ToAdminEmail()
    {
        $setting=\App\Setting::find(1);
        if($setting){
            $email=$setting->order_email;
        }else{
            $email="ventas@unimer.com.py";
        }

        $name="Admin";
        $username=auth()->user()->name;
        $description="There is a new Order From $username Please Check your Dashboard.";

        event(new MailEvent($name,$description,$email));
    }


    public function ToSellerEmail()
    {

        $name=auth()->user()->name;
        $description="We have received your order, soon we will process your order.";
        $email=auth()->user()->email;
        
        event(new MailEvent($name,$description,$email));
    }


    public function product_order(Request $request)
    {   
        dd($request->all());
    }

    private function sendNotification(Request $request)
    {
        $user=User::findOrFail($request->user_id);
        $product_type=ProductType::findOrFail($request->product_type_id);
        $product=Product::find($product_type->product_id);
        $description="<b>$user->name</b> order for the $request->amount $product->name";
        $order=collect([
            "description"=>$description
        ]);

        $admin=User::where('role','admin')->first();

        $admin->notify(new NewOrderNotification($order));
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order=Order::findOrFail($id);
        return view('backend.orders.edit',compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            "name"=>"required",
            "cost"=>"required",
            "current_stock"=>"required",
            "choice_options"=>"required",
            "user_id"=>"required"
        ]);
        $order=Order::findOrFail($id);
        $order->update($request->all());

        flash('order has been updated successfully')->success();

        return redirect()->route('orders.all');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order=Order::findOrFail($id);
        $order->delete();

        flash('order has been deleted successfully')->error();

        return redirect()->route('orders.all');
    }



    public function set_status(Request $request)
    {
        if(!isset($request->status)){
            flash('order amount is required')->error();    
            return redirect()->route('orders.all');    
        }
        $order=Order::findOrFail($request->id);
        
        if($request->status=='sent'){

            $productType=ProductType::findOrFail($order->product_type_id);
            
            if($request->type=="new")
            {
                $productType->quantity+=$order->amount;
                $productType->save();

            }else if($request->type=="send"){
                if($productType->quantity<$order->amount)
                {
                    flash("Not enough Stock Available to send this order")->error();
                    return redirect()->route('orders.all');            
                }
                $productType->quantity-=$order->amount;
                $productType->save();
            }
        }

        $this->clientNotification($request,$order);
        $order->status=$request->status;
        $order->save();
        flash("Status of Order has been Changed")->success();

        return ($request->type=='new')? redirect()->route('orders.new.all') : redirect()->route('orders.all');
    }

    private function clientNotification(Request $request,$order)
    {
        $admin=User::where('role','admin')->first();
        $user=User::findOrFail($order->user_id);

        $product_type=ProductType::findOrFail($order->product_type_id);
        $product=Product::find($product_type->product_id);
        $description="<b>$admin->name</b> order for the $request->amount $product->name has been $request->status";

        $order=collect([
            "description"=>$description
        ]);

        $user->notify(new NewOrderNotification($order));
    }

    public function mail()
    {
        
    }
}
