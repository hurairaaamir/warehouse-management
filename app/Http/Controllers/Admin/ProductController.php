<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\ProductType;
use Intervention\Image\Facades\Image;
use Helper;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Helper::isAdmin()){
            $products=Product::all();
        }else{
            $products=Product::where('user_id',auth()->id());
            $products=$products->paginate(8);
        }
        return view('backend.products.index',compact('products'));
    }

    public function api(){
        // dd('hee');
        $products=Product::all();
        // $products=collect($products);
        // return $products;
        // dd($products);
        // $data=view('backend.products.partials.product',compact('products'))->render();
        return response()->json([$products]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name"=>"required",
            "cost"=>"required",
            "user_id"=>"required",
            "image"=>'required|image|mimes:jpg,png,jpeg|max:4000',
            "productType"=>"required"
        ]);

        $image=$this->storeImage($request->image);
        $data=$request->all();
        $data=array_merge($data,compact('image'));
        $product=Product::create($data);

        foreach($request->productType as $productType){
            $product_id=$product->id;
            $productType=array_merge($productType,compact('product_id'));
            ProductType::create($productType);
        }
        
        flash('Product has been created successfully')->success();

        return redirect()->route('products.all');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    protected function storeImage($image)
    {
        $image_name = uniqid().'.'.$image->getClientOriginalExtension();
        $destinationPath=public_path(). '/images/';
        $img = Image::make($image->getRealPath());
        $img->resize(500, 500, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$image_name);
        
        $path='/images/'.$image_name;

        return $path;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$type)
    {
        $product=Product::findOrFail($id);
        $type_of_order=$type;
        return view('backend.products.show',compact('type_of_order','product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=Product::findOrFail($id);
        return view('backend.products.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $request->validate([
            "name"=>"required",
            "cost"=>"required",
            "user_id"=>"required"
        ]);
        $product=Product::findOrFail($id);

        if($request->image){
            $request->validate(["image"=>'required|image|mimes:jpg,png,jpeg|max:4000']);
            // $this->delete_img($product->image);
            $image=$this->storeImage($request->image);
            $product->image=$image;
        }
        $product->name=$request->name;
        $product->cost=$request->cost;
        $product->choice_options=$request->choice_options;
        $product->description=$request->description;
        $product->save();
        
            
        foreach($request->productType as $type)
        {
            if(isset($type["id"])){
                $productType=ProductType::findOrFail($type["id"]);
                $productType->update($type);
            }else{
                $product_id=$product->id;
                $type=array_merge($type,compact('product_id'));
                ProductType::create($type);   
            }
        }


        $product_sizes=$product->product_type->pluck('size')->toArray();
        $request_sizes=[];
        foreach($request->productType as $type){
            array_push($request_sizes,$type["size"]);
        }


        foreach($product_sizes as $size)
        {
            if(!in_array($size,$request_sizes)){
                $selected=ProductType::where('product_id',$product->id)->where('size',$size)->first();
                if($selected->orders->count()>0){
                    flash('you are trying to delete product size '.$selected->product->name.' with orders. you will have to delete orders first')->error();
                    return redirect()->route('products.all');   
                }else{
                    $selected->delete();
                }
            }
        }
        

        flash('Product has been updated successfully')->success();

        return redirect()->route('products.all');   
    }
    private function delete_img($path){
        $path=public_path($path);                    
        @unlink($path);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=Product::findOrFail($id);
        // $this->delete_img($product->image);
        $product->delete();

        flash('Product has been deleted successfully')->error();

        return redirect()->route('products.all');
    }


    public function setAmount(Request $request)
    {
        if(!isset($request->current_stock)){
            flash('product amount is required')->error();    
            return redirect()->route('products.all');    
        }

        $productType=ProductType::findOrFail($request->product_type_id);
        $productType->quantity=$request->current_stock;
        $productType->save();

        flash('Product Amount has been updated')->success();

        return redirect()->route('products.all');
    }

    public function sku_combination(Request $request)
    {
        $productTypes=json_decode($request->choic_options);
        $combinations = count($productTypes);
        $product=Product::find($request->product_id);
        if($product){
            return view('backend.products.partials.sku_combinations_edit', compact('combinations','productTypes','product'));    
        }
        return view('backend.products.partials.sku_combinations', compact('combinations','productTypes','product'));
    }
}
