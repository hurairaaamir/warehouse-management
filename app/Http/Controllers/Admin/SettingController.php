<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;

class SettingController extends Controller
{
    public function index()
    {
        $setting=Setting::find(1);
        return view('backend.settings.index',compact('setting'));
    }


    public function store(Request $request)
    {
        $setting=Setting::find(1);
        if($setting)
        {
            $setting->update($request->all());
        }else{
            Setting::create($request->all());
        }

        flash('Order Email has been updated')->success();

        return redirect()->route('settings');
    }
}
