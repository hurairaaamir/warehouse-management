<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    public function edit()
    {
        $user=auth()->user();
        return view('backend.user.edit',compact('user'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255',],
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);

        $user=auth()->user();
        $collection=collect($request->all());
        $password=Hash::make($collection["password"]);
        unset($collection["password"]);
        $merge=$collection->merge(compact('password'));
        $user->update($merge->all());

        flash('client has been updated successfully')->success();

        return redirect()->route('clients.all');
    }
}
