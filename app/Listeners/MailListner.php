<?php

namespace App\Listeners;

use App\Events\MailEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

use Mail;

class MailListner
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MailEvent  $event
     * @return void
     */
    public function handle(MailEvent $event)
    {
        $mail = new PHPMailer(true);

        try {

            //Server settings
            $mail->SMTPDebug = 0;                      // Enable verbose debug output
            $mail->SMTPDebug = 2;                      // Enable verbose debug output

            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = 'a2plcpnl0817.prod.iad2.secureserver.net';                    // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'warehouse@unimer.com.py';                     // SMTP username
            $mail->Password   = '1^/T]U;?Dj';                               // SMTP password
            // $mail->SMTPAutoTLS = false;
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->SMTPSecure='ssl';
            $mail->Port       =  465;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('warehouse@unimer.com.py', 'Warehouse');
            $mail->addAddress($event->email);     // Add a recipient
            // $mail->addAddress('your-recipient@gmail.com');               // Name is optional
            
            

            $mail->isHTML(true);

            $mail->Subject = 'Contact';

            $mail->Body    = view('emails.order_created',[
                "name"=>$event->name,
                "description"=>$event->description
            ])->render();

            $mail->send();

        } catch (Exception $e) {
            print_r($e);
        }
    }
}
