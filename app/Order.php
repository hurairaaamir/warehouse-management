<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable=['amount','type','status','user_id','product_type_id','price'];


    public function user(){
        return $this->belongsTo(User::class);
    }
    
    public function product_type(){
        return $this->belongsTo(ProductType::class);
    }
}
