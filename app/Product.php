<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=['name','cost','description','choice_options','user_id',"image"];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function product_type(){
        return $this->hasMany(ProductType::class);
    }
}
