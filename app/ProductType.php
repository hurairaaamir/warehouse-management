<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $fillable=['size','quantity','product_id'];

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

}
