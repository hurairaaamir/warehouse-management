<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        "address" => $faker->randomElement(['street 4th house 55','street 30 house 20','street 3rd house 7']),
        "phone"=>rand(),
        'tax_id'=>rand(),
        'contact_info'=>'random contact info',
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->paragraph(1),
        "cost" => rand(100,1000),
        "image"=>$faker->randomElement(["/images/1.jpeg","/images/2.jpeg","/images/3.jpeg","/images/4.jpeg","/images/5.jpg",'/images/6.jpg']),
        'choice_options'=>'[{"value":"12"},{"value":"20"}]',
        'user_id'=>2,
    ];
});