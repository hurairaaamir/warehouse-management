<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Product;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => '$2y$10$imU.Hdz7VauIT3LIMCMbsOXvaaTQg6luVqkhfkBcsUd.SJW2XSRKO',
                'role'           => 'admin',
                'remember_token' => null,
                'created_at'     => '2019-04-15 19:13:32',
                'updated_at'     => '2019-04-15 19:13:32',
            ],
            [
                'id'             => 2,
                'name'           => 'Selller',
                'email'          => 'seller@seller.com',
                'password'       => '$2y$10$imU.Hdz7VauIT3LIMCMbsOXvaaTQg6luVqkhfkBcsUd.SJW2XSRKO',
                'role'           => 'seller',
                'remember_token' => null,
                'created_at'     => '2019-04-15 19:13:32',
                'updated_at'     => '2019-04-15 19:13:32',
            ]
        ];
        User::insert($users);

        factory(User::class,30)->create();
        factory(Product::class,10)->create();


        $products=\App\Product::all();
        foreach($products as $product)
        {
            \App\ProductType::create([
                "size"=>"Large",
                "quantity"=>"12",
                "product_id"=>$product->id
            ]);
            \App\ProductType::create([
                "size"=>"Xtra Large",
                "quantity"=>"6",
                "product_id"=>$product->id
            ]);
            \App\ProductType::create([
                "size"=>"Small",
                "quantity"=>"9",
                "product_id"=>$product->id
            ]);
        }
    }
}
