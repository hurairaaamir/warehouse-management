<div class="aiz-sidebar-wrap">
    <div class="aiz-sidebar left c-scrollbar">
        <div class="aiz-side-nav-logo-wrap">
            <a class="d-block text-left">
                <img class="float-right" src="/uploads/all/shoping_bag.svg" class="brand-icon" style="float:left!important;">
                {{-- <img class="float-right" src="/assets/img/warehouse.png" class="brand-icon" style="float:left!important;float: left!important;width: 199px;height: 56px;margin: 10px;border-radius: 3px;"> --}}
                
                <h6 style="color:white;">Warehouse Management</h6>
            </a>
            
        </div>
        <div class="aiz-side-nav-wrap">
            <div class="px-20px mb-3">
                <input class="form-control bg-soft-secondary border-0 form-control-sm text-white" type="text" name="" placeholder="{{ ('Search in menu') }}" id="menu-search" onkeyup="menuSearch()">
            </div>
            <ul class="aiz-side-nav-list" id="search-menu">
            </ul>
            <ul class="aiz-side-nav-list" id="main-menu" data-toggle="aiz-side-menu">
                <li class="aiz-side-nav-item">
                    <a href="{{route('admin.dashboard')}}" class="aiz-side-nav-link">
                        <i class="las la-home aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Dashboard</span>
                    </a>
                </li>

                <!-- Clients-->
                @if(Helper::isAdmin())
                <li class="aiz-side-nav-item">
                    <a href="#" class="aiz-side-nav-link {{ Helper::areActiveRoutes(['clients.all', 'clients.create'])}}">
                        <i class="las la-tasks aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Clients</span>
                        <span class="aiz-side-nav-arrow"></span>
                    </a>
                    <ul class="aiz-side-nav-list level-2">
                        <li class="aiz-side-nav-item">
                            <a href="{{route('clients.all')}}" class="aiz-side-nav-link "">
                                <span class="aiz-side-nav-text">All Clients</span>
                            </a>
                        </li>
                        <li class="aiz-side-nav-item">
                            <a href="{{route('clients.create')}}" class="aiz-side-nav-link">
                                <span class="aiz-side-nav-text">Add Clients</span>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif
                {{--  --}}
                <!-- Product -->
                <li class="aiz-side-nav-item">
                    <a href="#" class="aiz-side-nav-link {{ Helper::areActiveRoutes(['products.all', 'products.create'])}}">
                        <i class="lab la-product-hunt aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Products</span>
                        <span class="aiz-side-nav-arrow"></span>
                    </a>
                    <!--Submenu-->
                    <ul class="aiz-side-nav-list level-2">
                        <li class="aiz-side-nav-item">
                            <a href="{{route('products.all')}}" class="aiz-side-nav-link">
                                <span class="aiz-side-nav-text">All Products</span>
                            </a>
                        </li>                            
                        @if(Helper::isAdmin())
                        <li class="aiz-side-nav-item">
                            <a class="aiz-side-nav-link" href="{{route('products.create')}}">
                                <span class="aiz-side-nav-text">Add New product</span>
                            </a>
                        </li>
                        @endif
                    </ul>
                </li>
                <li class="aiz-side-nav-item">
                    <a href="#" class="aiz-side-nav-link {{ Helper::areActiveRoutes(['orders.all', 'orders.create'])}}">
                        <i class="las la-shopping-cart aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text">Orders</span>
                        <span class="aiz-side-nav-arrow"></span>
                    </a>
                    <ul class="aiz-side-nav-list level-2">
                        <li class="aiz-side-nav-item">
                            <a href="{{route('orders.all')}}" class="aiz-side-nav-link">
                                <span class="aiz-side-nav-text">Current Inventory Orders</span>
                            </a>
                        </li>
                        <li class="aiz-side-nav-item">
                            <a href="{{route('orders.new.all')}}" class="aiz-side-nav-link">
                                <span class="aiz-side-nav-text">New Inventory Orders</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- Clients-->
                @if(Helper::isAdmin())
                <li class="aiz-side-nav-item">
                    <a href="#" class="aiz-side-nav-link {{ Helper::areActiveRoutes(['settings'])}}">
                        <i class="las la-cogs m-1 "></i>
                        <span class="aiz-side-nav-text">Settings</span>
                        <span class="aiz-side-nav-arrow"></span>
                    </a>
                    <ul class="aiz-side-nav-list level-2">
                        <li class="aiz-side-nav-item">
                            <a href="{{route('settings')}}" class="aiz-side-nav-link "">
                                <span class="aiz-side-nav-text">Order Email</span>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif

            </ul><!-- .aiz-side-nav -->
        </div><!-- .aiz-side-nav-wrap -->
    </div><!-- .aiz-sidebar -->
    <div class="aiz-sidebar-overlay"></div>
</div><!-- .aiz-sidebar -->
