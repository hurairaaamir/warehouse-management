@extends('backend.layouts.app')
@section('style')
<style>
  .my_badge{
    padding:5px;
    color:white;
    font-size: 14px;
    margin:3px;
    display: INLINE-BLOCK;
    text-align: center;
    width:100px;
    border-radius: 8px;
  }
</style>

@endsection
@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="row align-items-center">
		<div class="col-md-6">
			<h1 class="h3">{{('All orders')}}</h1>
    </div>
      {{-- <div class="col-md-6 text-md-right">
        <a href="{{ route('orders.create') }}" class="btn btn-circle btn-info">
          <span>{{('Add New orders')}}</span>
        </a>
      </div> --}}
	</div>
</div>

<div class="card">
    <div class="" >
      <div class="card-header row gutters-5">
        <div class="col text-center text-md-left">
          <h5 class="mb-md-0 h6">Orders</h5>
        </div>
      </div>
    </div>
    <div class="card-body">
        <table class="table-size" id="table_id" >
            <thead>
            <tr>
                <th>#</th>
                <th>Product Name</th>
                <th>Amount</th>
                <th>Price</th>
                <th>Size</th>
                @if(Helper::isAdmin())
                  <th>Client Name</th>
                @endif
                <th>Status</th>
                @if(Helper::isAdmin())
                  <th width="10%">Action</th>
                @endif
            </tr>
            </thead>
            <tbody>
             @foreach($orders as $key => $order)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$order->product_type->product->name}}</td>
                        <td>{{$order->amount}}</td>
                        <td>Gs. {{$order->price.".000"}}</td>
                        <td>{{$order->product_type->size}}</td>
                        @if(Helper::isAdmin())
                          <td>{{$order->user->name}}</td>
                        @endif
                        <td><span class="my_badge {{Helper::getColor($order->status)}}">{{$order->status=="in_process"?"In process":$order->status}}</span></td>

                        @if(Helper::isAdmin())
                          <td class="text-right footable-last-visible" style="display: table-cell;">    
                            @if($order->status!="sent"&&$order->status!="cancelled")   
                                <a href="#" class="btn btn-soft-primary btn-icon btn-circle btn-sm" onclick="showStatusModal('{{$order->id}}','{{$order->status}}')" title="Set Status">
                                    <i class="lab la-buffer"></i>
                                </a>
                            @endif
                              <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('orders.delete',$order->id)}}" title="Delete">
                                <i class="las la-trash"></i>
                              </a>
                          </td>
                        @endif
                    </tr>
              @endforeach 
            </tbody>
        </table>
    </div>
</div>

@endsection
@section('modal')
  <div class="modal fade" id="setAmount">
    <div class="modal-dialog modal-dialog-zoom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title fw-600">Set Order Amount</h6>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="p-3">
                    <form class="form-default" role="form" action="{{ route('orders.set_status') }}" method="POST">
                        @csrf
                        <input type="hidden" id="order_id" name="id">
                        <input type="hidden"  name="type" value="new">
                        <div class="form-group">
                          <select class="form-control aiz-selectpicker" name="status" id="status_id" data-live-search="true" required>
                              <option value="in_process">In Process</option>
                              <option value="pending">Pending</option>
                              <option value="sent">Sent</option>
                              <option value="cancelled">Canceled</option>
                          </select>
                        </div>
                        <div class="mb-5">
                            <button type="submit" class="btn btn-primary btn-block fw-600">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
  @include('modals.delete_modal')

@endsection

@section('script')
<script type="text/javascript" charset="utf8" src="/assets/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {

            $('#table_id').DataTable();
            var data=$('#table_id_filter label input').html();
            $('#table_id_filter label input').addClass("form-control form-control-sm");
            
        } );
        function update_approved(el){
            if(el.checked){
                var status = "active";
            }
            else{
                var status = "inactive";
            }
            $.post('', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    AIZ.plugins.notify('success', 'Approved sellers updated successfully');
                }
                else{
                    AIZ.plugins.notify('danger', 'Something went wrong');
                }
            });
        }

        function showStatusModal(id,status){
           $('#setAmount').modal();
           $('#order_id').val(id);
           $('#status_id').val(status);
        }
    </script>
@endsection
