@extends('backend.layouts.app')

@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
    <h5 class="mb-0 h6">{{('Edit Product')}}</h5>
</div>
<div class="col-md-10 mx-auto">
	<form class="form form-horizontal mar-top" action="{{route('products.update',$product->id)}}" method="POST" enctype="multipart/form-data" id="choice_form">
		@csrf
		<input type="hidden" name="product_id" value="{{$product->id}}">
		<div class="card">
			<div class="card-header">
				<h5 class="mb-0 h6">{{('Product Information')}}</h5>
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label class="col-md-3 col-from-label">Product Name <span class="text-danger">*</span></label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="name" placeholder="Name" value="{{$product->name}}" required>
						@if($errors->has('name'))
							<p class="help-block">
								{{ $errors->first('name') }}
							</p>
						@endif
					</div>
				</div>
				<div class="form-group row" id="category">
					<label class="col-md-3 col-from-label">Cost <span class="text-danger">*</span></label>
					<div class="col-md-8">
						<input type="number" class="form-control" step="0.01" min="0" name="cost" placeholder="Cost" value="{{$product->cost}}"  required>
						@if($errors->has('cost'))
							<p class="help-block">
								{{ $errors->first('cost') }}
							</p>
						@endif
					</div>
				</div>
				<div class="form-group row" id="brand">
					<label class="col-md-3 col-from-label">Description<span class="text-danger">*</span></label>
					<div class="col-md-8">
						<textarea  class="form-control" name="description" placeholder="description" required>{{$product->description}}</textarea>
						@if($errors->has('description'))
							<p class="help-block">
								{{ $errors->first('description') }}
							</p>
						@endif
					</div>
				</div>
				<div class="form-group row" id="brand">
					<label class="col-md-3 col-from-label">Select Client<span class="text-danger">*</span></label>
					<div class="col-md-8">
						<select class="form-control aiz-selectpicker" name="user_id" id="user_id" data-live-search="true">
							@foreach (\App\User::where('role','seller')->get() as $user)
								<option value="{{ $user->id }}" {{$product->user_id==$user->id}}>{{ $user->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group row" id="category">
					<label class="col-md-3 col-from-label">Product Images <span class="text-danger">*</span></label>
					<div class="col-md-8" style="cursor: pointer">
						<div class="input-group" id="image_div">
							<div class="input-group-prepend">
								<div class="input-group-text bg-soft-secondary font-weight-medium">Browse</div>
							</div>
							@php
								$image=NULL;
								if($product->image){
									$image=explode('/',$product->image);
									if(is_array($image)){
										$image=end($image);
									}
								}
							@endphp
							<div class="form-control file-amount">{{$image}}</div>
						</div>
					</div>
					@if($errors->has('image'))
						<p class="help-block">
							{{ $errors->first('image') }}
						</p>
					@endif
				</div>
				<input type="file" name="image" id="image"  style="display:none">
			</div>
		</div>
		<div class="card">
			<div class="card-header">
				<h5 class="mb-0 h6">Product size + quantity</h5>
			</div>
			<div class="card-body">	
				<div class="form-group row">
					<label class="col-md-3 col-from-label">Size<span class="text-danger">*</span></label>
					<div class="col-md-8">
						<input type="text" class="form-control aiz-tag-input" value="{{$product->product_type->pluck('size')}}" name="choic_options" placeholder="Size" data-on-change="update_sku">
					</div>
				</div>
				<div class="sku_combination" id="sku_combination">

				</div>
			</div>
		</div>
		<div class="mb-3 text-right">
			<button type="submit" class="btn btn-primary">update</button>
		</div>
	</form>
</div>



@endsection
@section('script')

<script type="text/javascript">
	$('#image_div').on('click',function (){
		
		$("#image").click();
	});

	$('#image').on('change',function(event){
		$(".file-amount").html(event.target.files[0].name);
	});

	$(document).ready(function(){
		AIZ.plugins.tagify();
		update_sku()
	});

	function update_sku(){
		$.ajax({
		   type:"POST",
		   url:'{{route("products.get_table")}}',
		   data:$('#choice_form').serialize(),
		   success: function(data){
			   $('#sku_combination').html(data);
		   }
	   });
	}
</script>

@endsection
