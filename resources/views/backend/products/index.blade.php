@extends('backend.layouts.app')
@section('style')
<link rel="stylesheet" href="/assets/css/pagination.css">
<style>
  .center_out{
        display:flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
    }
    .spread_container{
        display:flex;
        justify-content: space-around;
        margin-bottom: 20px;
        
    }
  .product-container{
      display:flex;
      justify-content: flex-start;
      align-items: center;
      flex-wrap: wrap;
  }
  @media(max-width:760px){
    .product-container{
      display:flex;
      justify-content: center;
      align-items: center;
      flex-wrap: wrap;
    }   
  }
  .product-container .product{
    flex:0 1 200px;
    margin: 10px 14px;
  }
  .product-container .product .product-card{
      background-color:#FFFFFF;
      padding:10px 20px;
      height: 230px;
      border-radius: 10px;
  }
  .product-container .product .product-card span{
      margin-top:15px;
      font-weight: 500;
      font-size:15px;
  }
  .card-lower{
    border-radius: 10px;
    font-weight:500;
    font-size: 14px;
  }
  .product-container .product .product-card img{
    width: 100px;
    padding-top: 15px;
    height: auto;
  }
</style>
@endsection
@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="row align-items-center">
		<div class="col-md-6">
			<h1 class="h3">{{('All products')}}</h1>
        </div>
        @if(Helper::isAdmin())
            <div class="col-md-6 text-md-right">
                <a href="{{ route('products.create') }}" class="btn btn-circle btn-info">
                <span>{{('Add New products')}}</span>
                </a>
            </div>
        @endif
	</div>
</div>
@if(!Helper::isAdmin())
    <div class="product-container">
        @foreach($products as $key => $product)
            <div class="product">
                <div class="center_out product-card text-center">
                    <img src="{{$product->image}}" alt="">
                    <span>{{$product->name}}</span>
                    <span class="text-right footable-last-visible">   
                        {{-- onclick="sendOrderModel({{$product->id}},'send')" --}}
                        <a href='/products/show/{{$product->id}}/send' class="btn btn-soft-primary btn-icon btn-circle btn-sm"  title="Order">
                            <i class="lab la-buffer"></i>
                        </a>
                        {{-- onclick="sendOrderModel({{$product->id}},'new')" --}}
                        <a href='/products/show/{{$product->id}}/new' class="btn btn-soft-success btn-icon btn-circle btn-sm" title="new Order">
                            <i class="lab la-buffer"></i>
                        </a>
                    </span>
                </div>
                <div class="card p-2 mt-3 text-center card-lower">
                    @php
                        $stock=0;
                        foreach($product->product_type as $p){
                            $stock=$p->quantity+$stock;
                        }
                    @endphp
                    
                    <span class="opacity-70">Total {{$stock}} in Stock</span>
                </div>
            </div>
        @endforeach
    </div>
    <div class="aiz-pagination mb-4">
        {{ $products->appends(request()->input())->links() }}
    </div>
@endif
@if(Helper::isAdmin())
    <div class="card">
        <form class="" id="sort_products" action="" method="GET">
        <div class="card-header row gutters-5">
            <div class="col text-center text-md-left">
            <h5 class="mb-md-0 h6">{{ ('products') }}</h5>
            </div>
        </div>
        </form>
        <div class="card-body">
            <table class="table-size" id="table_id" >
                <thead>
                <tr>
                    <th>#</th>
                    <th style="min-width:167px;">Name</th>
                    <th>Description</th>
                    <th style="min-width:100px">Cost</th>
                    @if(Helper::isAdmin())
                    <th style="min-width:100px">Client Name</th>
                    @endif
                    <th width="10%">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $key => $product)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td style="min-width: 163px"><img src="{{$product->image}}" style="width:50px;height:50px;border-radius:50px;" alt=""> {{$product->name}}</td>
                            <td>{{$product->description}}</td>
                            <td>Gs. {{$product->cost.".000"}}</td>
                            @if(Helper::isAdmin())
                            <td>{{$product->user->name}}</td>
                            <td class="text-right footable-last-visible" style="display: table-cell;">
                                <a href="#" class="btn btn-soft-primary btn-icon btn-circle btn-sm" onclick="showCheckoutModal({{$product->id}},{{$product->product_type}})" title="Set Quantity">
                                    <i class="lab la-buffer"></i>
                                </a>
                                <a href="{{route('products.edit',$product->id)}}" class="btn btn-soft-warning btn-icon btn-circle btn-sm" title="Edit">
                                    <i class="las la-edit"></i>
                                </a>
                                <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('products.delete',$product->id)}}" title="Delete">
                                    <i class="las la-trash"></i>
                                </a>
                            </td>
                            @else
                            <td class="text-right footable-last-visible" style="display: table-cell;">   
                                <a href="#" class="btn btn-soft-primary btn-icon btn-circle btn-sm" onclick="sendOrderModel({{$product->id}},'send')" title="Order">
                                <i class="lab la-buffer"></i>
                                </a>
                                <a href="#" class="btn btn-soft-secondary btn-icon btn-circle btn-sm" onclick="sendOrderModel({{$product->id}},'new')" title="new Order">
                                <i class="lab la-buffer"></i>
                                </a>
                            </td>
                            @endif
                        </tr>
                @endforeach 
                </tbody>
            </table>



        </div>
    </div>
@endif


@endsection
@section('modal')

<div class="modal fade" id="setAmount">
    <div class="modal-dialog modal-dialog-zoom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title fw-600">Set Product Amount</h6>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="p-3">
                    <div class="spread_container" id="spread_container_id">
                        
                    </div>
                    <form class="form-default" role="form" action="{{ route('products.set_amount') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <select class='form-control' name='product_type_id' id="select_id">
                            </select>
                        </div>
                        <input type="hidden" id="product_id" name="id">
                        <div class="form-group">
                            <input type="number"  id="current_stock" name="current_stock" class="form-control h-auto form-control-lg" placeholder="amount">
                        </div>
                        <div class="mb-5">
                            <button type="submit" class="btn btn-primary btn-block fw-600">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
  
  <div class="modal fade" id="sendOrder">
    <div class="modal-dialog modal-dialog-zoom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title fw-600" id="modle_title"></h6>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="p-3">
                    <form class="form-default" role="form" action="{{ route('orders.store') }}" method="POST">
                        @csrf
                        <input type="hidden" id="order_product_id" name="product_id">
                        <input type="hidden" id="order_type" name="type">
                        <input type="hidden"  name="user_id" value="{{auth()->id()}}">
                        <div class="form-group">
                            <input type="number"  id="order_amount" name="amount" class="form-control h-auto form-control-lg" placeholder="amount" required>
                        </div>
                        <div class="mb-5">
                            <button type="submit" id="send_btn" class="btn btn-block fw-600">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
  @include('modals.delete_modal')

@endsection

@section('script')
<script type="text/javascript" charset="utf8" src="/assets/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#table_id').DataTable();
            var data=$('#table_id_filter label input').html();
            $('#table_id_filter label input').addClass("form-control form-control-sm");
            
        });

        function update_approved(el){
            if(el.checked){
                var status = "active";
            }
            else{
                var status = "inactive";
            }
            $.post('', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    AIZ.plugins.notify('success', 'Approved sellers updated successfully');
                }
                else{
                    AIZ.plugins.notify('danger', 'Something went wrong');
                }
            });
        }

        function showCheckoutModal(id,array)
        {
            var div=document.getElementById('select_id');
            var spread_container=document.getElementById('spread_container_id');
            spread_container.innerHTML="";
            div.innerHTML="";
            array.forEach(function(element,index) {
                spread_container.innerHTML+=`<div class="opacity-60 fs-80">${element.size} : ${element.quantity}</div>`
                div.innerHTML+=`<option value='${element.id}'>${element.size}</option>`
            });
            
           $('#setAmount').modal();
        }
        
        function sendOrderModel(id,type){
           $('#sendOrder').modal();
           $('#order_product_id').val(id);
           $('#order_type').val(type);
           if(type=="send"){
               $("#modle_title").html("Set Order Amount");
               $("#send_btn").addClass("btn-primary");
               $("#send_btn").removeClass("btn-secondary");
           }else{
                $("#modle_title").html("Set New Order Amount")
                $("#send_btn").addClass("btn-secondary");
                $("#send_btn").removeClass("btn-primary");
           }
        }
        
    </script>
@endsection
