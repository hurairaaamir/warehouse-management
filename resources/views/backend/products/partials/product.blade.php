@foreach($products as $key => $product)
<div class='product'>
    <div class='center_out product-card text-center'>
        <img src='{{$product->image}}' alt=''>
        <span>{{$product->name}}</span>
        <span class='text-right footable-last-visible'>   
            {{-- onclick=`sendOrderModel({{$product->id}},'send')` --}}
            <a href='/products/show/{{$product->id}}/send' class='btn btn-soft-primary btn-icon btn-circle btn-sm'  title='Order'>
            <i class='lab la-buffer'></i>
            </a>
            {{-- onclick=`sendOrderModel({{$product->id}},'new')` --}}
            <a href='/products/show/{{$product->id}}/new' class='btn btn-soft-secondary btn-icon btn-circle btn-sm' title='new Order'>
            <i class='lab la-buffer'></i>
            </a>
        </span>
    </div>
    <div class='card p-2 mt-3 text-center card-lower'>
        <span>{{$product->current_stock}} left in stock</span>
    </div>
</div>
@endforeach