@if($combinations > 0)
<table class="table table-bordered">
	<thead>
		<tr>
			<td class="text-center">
				<label for="" class="control-label">Variant</label>
			</td>
			<td class="text-center">
				<label for="" class="control-label">Size</label>
			</td>
			<td class="text-center">
				<label for="" class="control-label">Quantity</label>
			</td>
		</tr>
	</thead>
	<tbody>
		@foreach ($productTypes as $key => $type)
			<tr class="variant">
				<td>
					<label for="" class="control-label">{{ $key+1}}</label>
				</td>
				<td>
					<input type="hidden" name="productType[{{$key}}][size]" value="{{$type->value}}" class="form-control" required>
					{{$type->value}}
				</td>
				<td>
					<input type="number" name="productType[{{$key}}][quantity]" value="" min="0" step="1" class="form-control" required>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
@endif
