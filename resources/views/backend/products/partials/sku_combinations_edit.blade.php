@if($combinations > 0)
<table class="table table-bordered">
	<thead>
		<tr>
			<td class="text-center">
				<label for="" class="control-label">Variant</label>
			</td>
			<td class="text-center">
				<label for="" class="control-label">Size</label>
			</td>
			<td class="text-center">
				<label for="" class="control-label">Quantity</label>
			</td>
		</tr>
	</thead>
	@php
		$size_array=$product->product_type->pluck('size')->toArray();
		$inarray_size=[];
		foreach($productTypes as $type){
			array_push($inarray_size,$type->value);
		}
		$i=0;
	@endphp
	<tbody>
		@foreach ($product->product_type as $key => $type)
			@if(in_array($type->size,$inarray_size))
				<tr class="variant">
					<td>
						<label for="" class="control-label">{{ $i++ +1}}</label>
					</td>
					<td>
						<input type="hidden" name="productType[{{$i-1}}][id]" value="{{$type->id}}">
						<input type="hidden" name="productType[{{$i-1}}][size]" value="{{$type->size}}" class="form-control" required>
						{{$type->size}}
					</td>
					<td>
						<input type="number" name="productType[{{$i-1}}][quantity]" value="{{$type->quantity}}" min="0" step="1" class="form-control" required>
					</td>
				</tr>
			@endif
		@endforeach
		@foreach($productTypes as $type)
			@if(!in_array($type->value,$size_array))
				@php
					$i++;
				@endphp
				<tr class="variant">
					<td>
						<label for="" class="control-label">{{ $i}}</label>
					</td>
					<td>
						<input type="hidden" name="productType[{{$i-1}}][size]" value="{{$type->value}}" class="form-control" required>
						{{$type->value}}
					</td>
					<td>
						<input type="number" name="productType[{{$i-1}}][quantity]" value="" min="0" step="1" class="form-control" required>
					</td>
				</tr>
			@endif
		@endforeach
	</tbody>
</table>
@endif
