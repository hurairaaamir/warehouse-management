@extends('backend.layouts.app')

@section('content')
<section class="mb-4 pt-3">
    <div class="container">
        <div class="bg-white shadow-sm rounded p-3">
            <div class="row">
                <div class="col-xl-5 col-lg-6">
                    <div class="sticky-top z-3 row gutters-10 flex-row-reverse">
                        <div class="col">
                            <div class="aiz-carousel product-gallery" data-nav-for='.product-gallery-thumb' data-fade='true'>
                                <div class="carousel-box img-zoom rounded">
                                    <img
                                        class="img-fluid lazyload"
                                        src="{{$product->image}}"
                                        data-src="{{$product->image}}"
                                        onerror=""
                                    >
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-auto w-80px">
                            <div class="aiz-carousel carousel-thumb product-gallery-thumb" data-items='5' data-nav-for='.product-gallery' data-vertical='true' data-focus-select='true' data-arrows='true'>
                                <div class="carousel-box c-pointer border p-1 rounded">
                                    <img
                                        class="lazyload mw-100 size-50px mx-auto"
                                        src="{{$product->image}}"
                                        data-src="{{$product->image}}"
                                        onerror=""
                                    >
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
                @php
                    if(count($product->product_type)>0){
                        $type=$product->product_type->first();
                    }else{
                        $type=new \App\ProductType(); 
                    }
                @endphp
                <div class="col-xl-7 col-lg-6">
                    <div class="text-left">
                        <span class="badge badge-md badge-inline badge-pill {{($type_of_order=="new")?'badge-success':'badge-primary'}}" style="float:right">{{($type_of_order=="send")? "Order for Current Inventory":"Order for New Inventory"}}</span>
                        <h1 class="mb-2 fs-20 fw-600">
                            {{$product->name}}
                        </h1>

                        <hr>
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <small class="opacity-70">Description: </small>
                            </div>
                            <small class="opacity-50 fs-10">
                                {{$product->description}}
                            </small>
                        </div>

                        <div class="row no-gutters mt-3">
                            <div class="col-sm-2">
                                <div class="opacity-50 my-2">{{ ('Price')}}:</div>
                            </div>
                            <div class="col-sm-10">
                                <div class="fs-20 opacity-60">
                                    <span>
                                        Gs. {{$product->cost.".000"}}
                                    </span>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <form id="option-choice-form" action="{{ route('orders.store') }}" method="POST">
                            @csrf
                            {{-- <input type="hidden" name="product_id" value="{{$product->id}}"> --}}

                            <input type="hidden" name="type" value="{{$type_of_order}}">
                            <input type="hidden" name="user_id" value="{{auth()->id()}}">
                            @if ($product->product_type != null)
                                <div class="row no-gutters">
                                    <div class="col-sm-2">
                                        <div class="opacity-50 my-2">Size:</div>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="aiz-radio-inline">
                                            @foreach ($product->product_type as $key => $product_type)
                                            <label class="aiz-megabox pl-0 mr-2">
                                                <input
                                                    type="radio"
                                                    name="product_type_id"
                                                    value="{{$product_type->id}}"
                                                    onclick="sizeChange(event,{{$product_type->id}})"
                                                    {{($key==0)? 'checked':''}}
                                                >
                                                <span class="aiz-megabox-elem rounded d-flex align-items-center justify-content-center py-2 px-3 mb-2">
                                                    {{ $product_type->size}}
                                                </span>
                                            </label>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif


                            <!-- Quantity + Add to cart -->
                            <div class="row no-gutters">
                                <div class="col-sm-2">
                                    <div class="opacity-50 my-2">Quantity:</div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="product-quantity d-flex align-items-center">
                                        <div class="row no-gutters align-items-center aiz-plus-minus mr-3" style="width: 130px;">
                                            <button class="btn col-auto btn-icon btn-sm btn-circle btn-light" type="button" onclick="quantityBtn('minus')" >
                                                <i class="las la-minus"></i>
                                            </button>
                                            <div class="col border-0 text-center flex-grow-1 fs-16 input-number" id="quantity_id">1</div>
                                            <input type="hidden" name="amount" id="amount_id" value="1">
                                            <button class="btn  col-auto btn-icon btn-sm btn-circle btn-light" type="button" onclick="quantityBtn('plus')">
                                                <i class="las la-plus"></i>
                                            </button>
                                        </div>
                                        <div class="avialable-amount opacity-60">(<span id="available-quantity">{{$type->quantity}}</span> available)</div>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row no-gutters pb-3" id="chosen_price_div">
                                <div class="col-sm-2">
                                    <div class="opacity-50 my-2">Total Price:</div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="product-price">
                                        <strong id="total_price" class="h4 fw-600 text-primary">
                                            Gs. {{$product->cost.".000"}}
                                        </strong>
                                    </div>
                                    <input type="hidden" name="price" id="price_id" value="{{$product->cost}}">
                                </div>
                            </div>
                        </form>
                            <div class="mt-3">
                                <button type="button" class="btn btn-soft-primary mr-2 add-to-cart fw-600" data-toggle="modal" data-target="#confirmModal">
                                    <i class="las la-shopping-bag"></i>
                                    <span class="d-none d-md-inline-block"> Create Order</span>
                                </button>
                                
                                {{-- <button type="button" class="btn btn-primary buy-now fw-600" >
                                    <i class="la la-shopping-cart"></i> {{ ('Buy Now')}}
                                </button> --}}
                            </div>                  
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>


  
  <!-- Modal -->
  <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="confirmModalLabel">Confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <p>Are you sure? You are about to submit this order.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-link mt-2" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary mt-2" onclick="submitForm()">Confirm</button>
        </div>
      </div>
    </div>
  </div>

@include('modals.confirm')

@endsection

@section('script')
<script type="text/javascript">
    var pausecontent = new Array();
    var product_price={{$product->cost}};
    var type_of_order="{{$type_of_order}}";
    @foreach($product->product_type as $type)
        pausecontent.push({
                "id":"{{$type->id}}",
                "size":"{{$type->size}}",
                "quantity":"{{$type->quantity}}"
            });
    @endforeach

	$(document).ready(function(){
		AIZ.plugins.tagify();
	});

	$('#image_div').on('click',function (){
		$("#image").click();
	});

	function sizeChange(event,id)
    {
        var index=pausecontent.findIndex((u) => u.id==id);
        $("#available-quantity").html(pausecontent[index].quantity);

        $("#total_price").html("Gs. "+product_price+".000");
        $('#amount_id').val(product_price);

        document.getElementById('quantity_id').innerHTML=1;
    }

    function quantityBtn(type)
    {
        var quantity=$('#quantity_id').html();
        var quantity_input=document.getElementById('quantity_id');
        var available_quantity=$("#available-quantity").html();

        (type=='plus')?quantity++:quantity--;
        if(type_of_order=='new'){
            if(quantity>0){
                quantity_input.innerHTML=quantity;
                $('#amount_id').val(quantity);
                $("#total_price").html("Gs. "+(product_price*quantity)+".000");
                $('#price_id').val("Gs. "+(product_price*quantity)+".000");
            }
        }else{
            if(quantity>0&&available_quantity>=quantity){
                quantity_input.innerHTML=quantity;
                $('#amount_id').val(quantity);
                $("#total_price").html("Gs. "+(product_price*quantity)+".000");
                $('#price_id').val("Gs. "+(product_price*quantity)+".000");

            }
        }

    }
    function submitForm()
    {
        $("#option-choice-form").submit();
    }
	$('#image').on('change',function(event){
		$(".file-amount").html(event.target.files[0].name);
	});

    $('#size-id').on('change',function(event){
		$(".file-amount").html(event.target.files[0].name);
	});

	function update_sku(){
		$.ajax({
		   type:"POST",
		   url:'{{route("products.get_table")}}',
		   data:$('#choice_form').serialize(),
		   success: function(data){
			   $('#sku_combination').html(data);
		   }
	   });
	}
</script>

@endsection
