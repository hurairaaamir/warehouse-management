@extends('backend.layouts.app')

@section('content')
@php
  if(!$setting){
    $setting=new \App\Setting();
  }
@endphp
<div class="col-md-10 mx-auto">
	<form class="form form-horizontal mar-top" action="{{route('settings.store')}}" method="POST" enctype="multipart/form-data" id="choice_form">
		@csrf
		<div class="card">
			<div class="card-header">
				<h5 class="mb-0 h6">{{('Setting')}}</h5>
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label class="col-md-3 col-from-label">Order Email <span class="text-danger">*</span></label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="order_email" placeholder="order email" value="{{$setting->order_email}}" required>
						@if($errors->has('order_email'))
							<p class="help-block">
								{{ $errors->first('order_email') }}
							</p>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="mb-3 text-right">
			<button type="submit" name="button" class="btn btn-primary">Save</button>
		</div>
	</form>
</div>



@endsection
@section('script')

<script type="text/javascript">
    function add_more_customer_choice_option(i, name){
        $('#customer_choice_options').append('<div class="form-group row"><div class="col-md-3"><input type="hidden" name="choice_no[]" value="'+i+'"><input type="text" class="form-control" name="choice[]" value="'+name+'" placeholder="{{ ('Choice Title') }}" readonly></div><div class="col-md-8"><input type="text" class="form-control aiz-tag-input" name="choice_options_'+i+'[]" placeholder="{{ ('Enter choice values') }}" data-on-change="update_sku"></div></div>');

    	AIZ.plugins.tagify();
    }

	$('input[name="colors_active"]').on('change', function() {
	    if(!$('input[name="colors_active"]').is(':checked')){
			$('#colors').prop('disabled', true);
		}
		else{
			$('#colors').prop('disabled', false);
		}
		update_sku();
	});

	$('#colors').on('change', function() {
	    update_sku();
	});

	$('input[name="unit_price"]').on('keyup', function() {
	    update_sku();
	});

	$('input[name="name"]').on('keyup', function() {
	    update_sku();
	});

	function delete_row(em){
		$(em).closest('.form-group row').remove();
		update_sku();
	}

    function delete_variant(em){
		$(em).closest('.variant').remove();
	}

	function update_sku(){
		$.ajax({
		   type:"POST",
		   url:'',
		   data:$('#choice_form').serialize(),
		   success: function(data){
			   $('#sku_combination').html(data);
			   if (data.length > 1) {
				   $('#quantity').hide();
			   }
			   else {
					$('#quantity').show();
			   }
		   }
	   });
	}

	$('#choice_attributes').on('change', function() {
		$('#customer_choice_options').html(null);
		$.each($("#choice_attributes option:selected"), function(){
            add_more_customer_choice_option($(this).val(), $(this).text());
        });
		update_sku();
	});


</script>

@endsection
