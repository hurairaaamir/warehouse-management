@extends('backend.layouts.app')

@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
    <h5 class="mb-0 h6">{{('Add New user')}}</h5>
</div>
<div class="col-md-10 mx-auto">
	<form class="form form-horizontal mar-top" action="{{route('user.update')}}" method="POST" enctype="multipart/form-data" id="choice_form">
		@csrf
		<div class="card">
			<div class="card-header">
				<h5 class="mb-0 h6">{{('user Information')}}</h5>
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label class="col-md-3 col-from-label">user Name <span class="text-danger">*</span></label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="name" placeholder="Name" value="{{$user->name}}" required>
						@if($errors->has('name'))
							<p class="help-block">
								{{ $errors->first('name') }}
							</p>
						@endif
					</div>
				</div>
				<div class="form-group row" id="category">
					<label class="col-md-3 col-from-label">Email <span class="text-danger">*</span></label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="email" placeholder="Email" value="{{$user->email}}"  required>
						@if($errors->has('email'))
							<p class="help-block">
								{{ $errors->first('email') }}
							</p>
						@endif
					</div>
				</div>
				<div class="form-group row" id="brand">
					<label class="col-md-3 col-from-label">Password<span class="text-danger">*</span></label>
					<div class="col-md-8">
						<input type="password" class="form-control" name="password" placeholder="password"  required>
						@if($errors->has('password'))
							<p class="help-block">
								{{ $errors->first('password') }}
							</p>
						@endif
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3 col-from-label">Confirm Password<span class="text-danger">*</span></label>
					<div class="col-md-8">
						<input type="password" class="form-control" name="password_confirmation" placeholder="password"  required>
						@if($errors->has('password_confirmation'))
							<p class="help-block">
								{{ $errors->first('password_confirmation') }}
							</p>
						@endif
					</div>
				</div>

				<div class="form-group row" id="brand">
					<label class="col-md-3 col-from-label">Address</label>
					<div class="col-md-8">
						<input type="address" class="form-control" name="address" placeholder="Address" value="{{$user->address}}">
					</div>
                </div>
                <div class="form-group row" id="brand">
					<label class="col-md-3 col-from-label">Tax id</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="tax_id" placeholder="Tax id" value="{{$user->tax_id}}">
					</div>
				</div>
				<div class="form-group row" id="brand">
					<label class="col-md-3 col-from-label">Contact info</label>
					<div class="col-md-8">
						<textarea  class="form-control" name="contact_info" placeholder="contact info" >{{$user->contact_info}}</textarea>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-md-3 col-from-label">Phone Number</label>
					<div class="col-md-6">
						<input type="number" min="0" value="{{$user->phone}}"  placeholder="Phone Number" name="phone" class="form-control">
					</div>
				</div>
			</div>
		</div>
		<div class="mb-3 text-right">
			<button type="submit" class="btn btn-primary">Save</button>
		</div>
	</form>
</div>



@endsection
@section('script')

<script type="text/javascript">
    function add_more_customer_choice_option(i, name){
        $('#customer_choice_options').append('<div class="form-group row"><div class="col-md-3"><input type="hidden" name="choice_no[]" value="'+i+'"><input type="text" class="form-control" name="choice[]" value="'+name+'" placeholder="{{ ('Choice Title') }}" readonly></div><div class="col-md-8"><input type="text" class="form-control aiz-tag-input" name="choice_options_'+i+'[]" placeholder="{{ ('Enter choice values') }}" data-on-change="update_sku"></div></div>');

    	AIZ.plugins.tagify();
    }

	$('input[name="colors_active"]').on('change', function() {
	    if(!$('input[name="colors_active"]').is(':checked')){
			$('#colors').prop('disabled', true);
		}
		else{
			$('#colors').prop('disabled', false);
		}
		update_sku();
	});

	$('#colors').on('change', function() {
	    update_sku();
	});

	$('input[name="unit_price"]').on('keyup', function() {
	    update_sku();
	});

	$('input[name="name"]').on('keyup', function() {
	    update_sku();
	});

	function delete_row(em){
		$(em).closest('.form-group row').remove();
		update_sku();
	}

    function delete_variant(em){
		$(em).closest('.variant').remove();
	}

	function update_sku(){
		$.ajax({
		   type:"POST",
		   url:'',
		   data:$('#choice_form').serialize(),
		   success: function(data){
			   $('#sku_combination').html(data);
			   if (data.length > 1) {
				   $('#quantity').hide();
			   }
			   else {
					$('#quantity').show();
			   }
		   }
	   });
	}

	$('#choice_attributes').on('change', function() {
		$('#customer_choice_options').html(null);
		$.each($("#choice_attributes option:selected"), function(){
            add_more_customer_choice_option($(this).val(), $(this).text());
        });
		update_sku();
	});


</script>

@endsection
