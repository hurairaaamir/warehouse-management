<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');







Route::get('/admin', 'HomeController@admin_dashboard')->name('admin.dashboard');

Route::get('/user/edit',"UserController@edit")->name('user.edit');
Route::post('/user/update',"UserController@update")->name('user.update');
// ->middleware(['auth', 'admin']);
// , 'admin'
// ->middleware('admin');
Route::group(['prefix' =>'', 'middleware' => ['auth']], function(){

    Route::get('/products/all','Admin\ProductController@index')->name('products.all');
    Route::get('/products/create','Admin\ProductController@create')->name('products.create');
    Route::post('/products/store','Admin\ProductController@store')->name('products.store');
    Route::get('/products/delete/{id}','Admin\ProductController@destroy')->name('products.delete');
    Route::get('/products/edit/{id}','Admin\ProductController@edit')->name('products.edit');
    Route::post('/products/update/{id}','Admin\ProductController@update')->name('products.update');
    Route::post('/products/set_amount','Admin\ProductController@setAmount')->name('products.set_amount');
    Route::post('products/api','Admin\ProductController@setAmount')->name('products.api');
    Route::post('products/get-table','Admin\ProductController@sku_combination')->name('products.get_table');
    Route::get('/products/show/{id}/{type}','Admin\ProductController@show');


    Route::get('/clients/all','Admin\ClientController@index')->name('clients.all');
    Route::get('/clients/create','Admin\ClientController@create')->name('clients.create');
    Route::post('/clients/store','Admin\ClientController@store')->name('clients.store');
    Route::get('/clients/delete/{id}','Admin\ClientController@destroy')->name('clients.delete');
    Route::post('/clients/approved','Admin\ClientController@updateStatus')->name('clients.approved');
    Route::get('/clients/edit/{id}','Admin\ClientController@edit')->name('clients.edit');
    Route::post('/clients/update/{id}','Admin\ClientController@update')->name('clients.update');
    

    Route::get('/orders/all','Admin\OrderController@index')->name('orders.all');
    Route::get('/orders/new/all','Admin\OrderController@new_index')->name('orders.new.all');
    Route::get('/orders/create','Admin\OrderController@create')->name('orders.create');
    Route::post('/orders/store','Admin\OrderController@store')->name('orders.store');
    Route::get('/orders/delete/{id}','Admin\OrderController@destroy')->name('orders.delete');
    Route::get('/orders/edit/{id}','Admin\OrderController@edit')->name('orders.edit');
    Route::post('/orders/update/{id}','Admin\OrderController@update')->name('orders.update');
    Route::post('/orders/set_status','Admin\OrderController@set_status')->name('orders.set_status');
    Route::post('/product/amount','Admin\OrderController@product_order')->name('products.order');


    Route::post('/mark/notification','HomeController@mark_notification')->name('mark.notification'); 

    Route::get('/settings/index','Admin\SettingController@index')->name('settings')->middleware('admin');
    Route::post('/settings/store','Admin\SettingController@store')->name('settings.store')->middleware('admin');
});

Route::get('/test','Admin\OrderController@ToAdminEmail');

